-module(erlang6).

-export([main/2]).

main(List, Word) ->
	search(List,Word)
.

search([{Word, Value}|_], Word) -> 
Value;


search([_|Tail], Word) -> 
	search(Tail, Word)
.