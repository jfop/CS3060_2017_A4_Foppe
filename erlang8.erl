-module(erlang8).

-export([main/1]).
-export([fact/1]).

main(Num) ->

	
	MyNum = fact(Num),
	io:fwrite("~w",[MyNum])
.

fact(Num) when Num == 0 -> 1;
fact(Num) when Num > 0 -> Num*fact(Num -1 ).