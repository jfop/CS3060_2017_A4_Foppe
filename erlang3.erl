-module(erlang3).

-export([getNum/1]).

getNum(String)
  -> getNum(String, 0)
.

getNum([], Num)
  -> Num
;

getNum([Head], Num)
  when (Head =/= ($ ))-> 
  	Num + 1;

getNum([Head, Tail | Rest], Num)
  when ((Head =/= ($ )) andalso (Tail =:= ($ )))
  -> getNum([Tail | Rest], Num + 1)
;

getNum([_ | Rest], Num)
  -> getNum(Rest, Num)
.