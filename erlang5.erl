-module(erlang5).

-export([main/1]).

main(success) -> "success~n";
main({error, Message}) -> "error: ~s~n" ++ Message.