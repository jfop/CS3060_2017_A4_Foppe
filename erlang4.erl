-module(erlang4).

-export([main/1]).

main(0) -> 0;

main(1) -> 1;

main(N) -> main(N-1) + main(N-2).