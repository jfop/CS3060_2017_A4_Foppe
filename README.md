c(erlang1).
erlang1:hello_world().

c(erlang2).
erlang2:getNum("This is a test string").

c(erlang3).
erlang3:getNum("This is a test string").

c(erlang4).
erlang4:main(5).

c(erlang5).
erlang5:main({error, " the error was x"}).

c(erlang6).
erlang6:main([{erlang, "A functional language"}, {ruby, "an OO language"}]).


c(erlang8).
erlang:main(10).

c(erlang9).
erlang9:start(5).

c(erlang10).
erlang10:start(5).