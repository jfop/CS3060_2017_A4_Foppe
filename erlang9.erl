-module(erlang9).

-export([start/1, mainProc/3, message1/0,message2/0]).

mainProc(N, Pid1, Pid2) ->

	Pid1 ! {forward, self()},
	receive 
		message1 ->
			io:format("One~n", [])
	end,
	mainProc(N - 1,Pid1, Pid2)
.

message1() ->
	receive

		{forward, Pid2} ->
			io:format("Two~n", []),
		    Pid2 ! message3,
            message1()
    end,
    receive
    	{back, Pid3} ->
    	io:format("Two~n", []),
		    Pid3 ! message1,
            message1()
    end	
.

message2() ->
	receive
		{message3, Pid2} ->
			io:format("Three~n", []),
            Pid2 ! back,
            message2()
    end
.



start(Num) ->
	Pid1 = spawn(erlang9, message1,[]),
	Pid2 = spawn(erlang9, message2,[]),
	spawn(erlang9, mainProc,[Num, Pid1, Pid2])
.