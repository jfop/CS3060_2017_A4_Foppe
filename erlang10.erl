-module(erlang10).

-export([start/1, mainProc/3, message1/0,message2/0]).







mainProc(N, Pid1, Pid2) ->

	%TOprocess (MESSAGE) {"functioncall", PassPid ID to talk to nxt one}
	Pid1 ! {message2, Pid2},
	receive
		message1 ->
			io:format("One~n", [])
	end,
	mainProc(N - 1,Pid1, Pid2)
.
message1() ->
	receive

		{message2, Pid2} ->
			io:format("Two~n", []),
			%?????????????????????????????????????????????????????????????? SEND NXT!!
		    %To PROCESS 2, callfunction message3 in pid2(which is func. message2)
		    Pid2 ! message3,
            message1()
    end
.
message2() ->
	receive
		{message3} ->
			io:format("Three~n", []),
            self() ! mainProc,
            message2()
    end
.




start(Num) ->
	Pid1 = spawn(erlang9, message1,[]),
	Pid2 = spawn(erlang9, message2,[]),
	spawn(erlang9, mainProc,[Num, Pid1, Pid2])
.